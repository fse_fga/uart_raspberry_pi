# Exemplo de acesso à UART em C

Este exemplo simples demonstra o uso da UART na Raspberry Pi utliziando a linguagem C.

## Compilar

Para compilar o exemplo basta executar o comando:

```
make
```

## Executando

```
bin/bin
```