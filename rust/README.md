### Descrição

Esse cógido em rust utiliza da biblioteca [RPPAL](https://docs.rs/rppal/latest/rppal/uart/struct.Uart.html#method.new) para fazer a comunição serial para mandar uma mensagem de "HELLO" via **tx** e receber via **rx** e exibi-la em tela.

### Laboratório

O teste consiste além de executar, mudar o tempo de espera entre o envio da mensagem e o recebimento na linha `sleep(Duration::from_micros(1_000_000));`.

### Como rodar

Com o rust já instalado (se não estiver use esse [link](https://www.rust-lang.org/pt-BR/tools/install))

Para rodar a aplicação execute:


```
cargo run
```
