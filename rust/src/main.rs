use std::str;
use std::thread::sleep;
use std::time::Duration;

use rppal::uart::{Parity, Uart};

const BAUD_RATE: u32 = 9600;
const DATA_BITS: u8 = 8;
const STOP_BITS: u8 = 1;

fn main() {
    // Abre porta serial
    // Nessa etapa já é configurado várias coisas sendo elas e pode ser visto em https://docs.rs/rppal/latest/src/rppal/uart.rs.html#422
    //    - Abre o arquivo, que por padrão é /dev/serial0 (para abrir com path diferente deve ser usado o with_path)
    //    - Configura o termios (todas as configs do uart)
    //    - Faz o flush das portas.
    let mut uarto = Uart::new(BAUD_RATE, Parity::None, DATA_BITS, STOP_BITS).unwrap();
	
    // Define restrições para o modo de leitura 
    // No caso ele bloqueia o READ mode até que tenha 1 byte disponível, ou enquanto o tempo decorre
    uarto.set_read_mode(1, Duration::from_secs(1)).unwrap();
	
    let envia = b"Hello"; // Cria mensagem bufferizada
    println!("Escreveu {} bytes", envia.len());
    uarto.write(envia).unwrap(); // Envia mensagem via tx

    sleep(Duration::from_secs(1));
    
    let mut leitura = vec![0; 255]; // Define 255 caracteres de leitura
    let bytes_lidos = uarto.read(&mut leitura).unwrap(); // Lê buffer 255 caracteres da porta rx
    println!("Leu {} bytes ", bytes_lidos);

    // Converte o buffer para string
    let to_str_buffer = str::from_utf8(&leitura[0..bytes_lidos]).expect("Não foi possível converter buffer para string").trim_matches(char::from(0));
    println!("Bytes da leitura: {}", to_str_buffer);

    // Fecha a porta serial
    drop(uarto);
}
