# Exemplos de uso da UART na Raspberry Pi

Este repositório contém exemplos de como utilizar a comunicação serial via UART na Raspberry Pi. 

Estão presentes exemplos nas linguagens C, Python e Rust.

## Configuração da interface de UART na Raspberry Pi

Inicie a raspberry pi, acesse via SSH execute o seguinte comando:

```
sudo raspi-config
```

Na tela de Configurações apresentada seleciona as seguintes opções:

1. Opção `3 Interface options`  
   
![](images/tela_1.png)

2. Opção `I6 - Serial Port`  

![](images/tela_2.png)

3. Login via Shell - `<No>`. Esta opção redireciona o terminal (entrada de teclado e saída de caracteres para a porta serial)  

![](images/tela_3.png)

4. Habilitar hardware da serial - `<Yes>`  

![](images/tela_4.png)

5. Confirmar configurações - `<Ok>`  

![](images/tela_5.png)

6. Finalizar configurações - `<Finish>`

![](images/tela_6.png)

Por fim, caso as configurações tenham sido mudadas, será necessário reiniciar a placa (`reboot`)